package india.consul3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonalDetailActivity extends AppCompatActivity {
    Button Next;
    EditText EMailed,MobileNo,ContactPerson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_detail);
        Next=(Button)findViewById(R.id.PersonalDetailNext);
        EMailed=(EditText)findViewById(R.id.PersonalDetailEdEmailId);
        MobileNo=(EditText)findViewById(R.id.EdPersonalDetailContactMobile);
        ContactPerson=(EditText)findViewById(R.id.EdContactPerson);
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String moblieNo=MobileNo.getText().toString();
                final String contact_per=ContactPerson.getText().toString();
                final String email=EMailed.getText().toString();
                if(contact_per.length()==0){
                    ContactPerson.setError("Invalid Contact name");
                }
                else{
                    if(moblieNo.length()==10){
                        if (isValidEmail(email)) {
                            Intent intent = new Intent(PersonalDetailActivity.this, FeedbackActivity.class);
                            startActivity(intent);
                        }
                        else{
                            EMailed.setError("Invalid Email");
                        }
                    }
                    else {
                        MobileNo.setError("Invalid Mobile no");
                    }
                }
            }
        });
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@+[a-zA-Z]+.+[a-z]";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
