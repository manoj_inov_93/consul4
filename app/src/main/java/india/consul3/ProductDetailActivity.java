package india.consul3;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductDetailActivity extends AppCompatActivity {
    Button Next;
    EditText SerialNumber, ConformSerialNumber;
    Boolean a = false;
    Spinner SelectProduct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        SerialNumber = (EditText) findViewById(R.id.ETProductNumber);
        ConformSerialNumber = (EditText) findViewById(R.id.EDConfirmSerialNumber);
        SelectProduct = (Spinner) findViewById(R.id.SelectProductSpinner);
        String[] plants = new String[]{
                "Select your product",
                "ac",
                "Transformer",
                "Voltage stabilizer",
                "Inverter",
                "Domestic inverter",
                "Solar"
        };
        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,R.layout.spinner_product,plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0) {
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_product);
        SelectProduct.setAdapter(spinnerArrayAdapter);
        SelectProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + "selected", Toast.LENGTH_LONG).show();
                    a = true;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Next = (Button) findViewById(R.id.ButtonProductDetailNext);
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String serial_no = SerialNumber.getText().toString();
                final String conform_ser_no = ConformSerialNumber.getText().toString();
                if (serial_no.equals(conform_ser_no) ) {
                    if(!serial_no.isEmpty()) {
                        if(a.equals(true)) {
                            Intent intent = new Intent(ProductDetailActivity.this, PersonalDetailActivity.class);
                            startActivity(intent);}
                        else {
                            TextView errorText = (TextView) SelectProduct.getSelectedView();
                            errorText.setError("select your product");
                        }
                    }
                    else {
                        SerialNumber.setError("serial number should not be empty");
                    }
                }
                else {
                    ConformSerialNumber.setError("wrong serial number");
                }
            }

        });

    }

}
