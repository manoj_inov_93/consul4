package india.consul3;
import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.content.Intent;
import  android.view.View;
import android.widget.TextView;


public class HomeActivity extends AppCompatActivity {
   Button RegisterYourProduct,ContactUs,ServiceRequestButton;
    TextView isconnevct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        RegisterYourProduct=(Button)findViewById(R.id.RegisterYourProductButton);
        ContactUs=(Button)findViewById(R.id.ContactUsButton);
        isconnevct=(TextView)findViewById(R.id.connect);
        ServiceRequestButton=(Button)findViewById(R.id.ServiceRequestButton);
        if (isconnevct()){
            isconnevct.setBackgroundColor(0xFF00CC00);
            isconnevct.setText("you are connected");
        }
        else{
            isconnevct.setText("you are not conneted ");

        }

        RegisterYourProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,ProductDetailActivity.class);
                startActivity(intent);
            }
        });
        ContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,ContactUsActivity.class);
                startActivity(intent);
            }
        });
        ServiceRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,ServiceRequestActivity.class);
                startActivity(intent);
            }
        });


    }
    public boolean isconnevct(){
        ConnectivityManager connmgr=(ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connmgr.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}

