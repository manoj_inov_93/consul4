package india.consul3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FeedbackActivity extends AppCompatActivity {
    EditText score;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        score=(EditText)findViewById(R.id.EdScore);
        submit=(Button)findViewById(R.id.FeedbackSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String scr = score.getText().toString();
                if (isValidEmail(scr)) {
                    Intent intent = new Intent(FeedbackActivity.this, DisplayDetail.class);
                    startActivity(intent);
                }
                else {
                    score.setError("please enter the score to our app between 0 to 9");
                }
            }
        });
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "[0-9]";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
