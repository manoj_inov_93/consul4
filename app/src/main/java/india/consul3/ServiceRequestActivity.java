package india.consul3;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.String;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceRequestActivity extends AppCompatActivity {
    Spinner SelectYourProductSpinner, SecectYourProblemSpinner, OthersSpinner;
    Boolean FlagProduct = false;
    Boolean FlagProblem = false;
    Boolean FlagSerialNo = false, FlagOthers = false;
    EditText MobileNo, serialNo;
    TextView ServiceRequestSerialNumber;
    Button submit;
    StringBuilder sb = new StringBuilder();
    String link = "http://wsuser:wspass@www.innoventestech.in/consul/backoffice/api/ServiceRequest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_request);
        SelectYourProductSpinner = (Spinner) findViewById(R.id.ServiceRequestSecectYourProduct);
        SecectYourProblemSpinner = (Spinner) findViewById(R.id.ServiceRequestSecectProblem);
        OthersSpinner = (Spinner) findViewById(R.id.spinner);
        submit = (Button) findViewById(R.id.ServiceRequestSubmit);
        MobileNo = (EditText) findViewById(R.id.ServiceRequestEdContactMobile);
        serialNo = (EditText) findViewById(R.id.ServiceRequestEdSerialNumber);
        ServiceRequestSerialNumber = (TextView) findViewById(R.id.ServiceRequestSerialNumber);
        OthersSpinner.setVisibility(View.GONE);
        serialNo.setVisibility(View.GONE);
        ServiceRequestSerialNumber.setVisibility(View.GONE);
        OthersSpinner.setEnabled(false);
        //InputStream inputStream = null;
        //String result = "";

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNo = MobileNo.getText().toString();
                String serial_no = serialNo.getText().toString();
                if (!FlagOthers) {
                    if (serial_no.length() > 0 || (!FlagSerialNo)) {
                        if (FlagProduct) {
                            if (FlagProblem) {
                                if (mobileNo.length() == 10) {
                                    MyAsyncTask obj=new MyAsyncTask();
                                    obj.execute(link);
                                    Intent intent = new Intent(ServiceRequestActivity.this, DisplayDetail.class);
                                    startActivity(intent);

                                } else {
                                    MobileNo.setError("wrong mobile number");
                                }
                            } else {
                                TextView errorText1 = (TextView) SecectYourProblemSpinner.getSelectedView();
                                errorText1.setError("select your product");
                            }
                        } else {
                            TextView errorText = (TextView) SelectYourProductSpinner.getSelectedView();
                            errorText.setError("select your product");
                        }
                    } else {
                        serialNo.setError("enter the serial number");
                    }
                } else {
                    TextView errorText = (TextView) OthersSpinner.getSelectedView();
                    errorText.setError("select your product");
                }
            }
        });
        String[] product = new String[]{
                "Select your product",
                "Inverter123",
                "ac1234",
                "Voltage stabilizer12345",
                "Voltage stabilizer123456",
                "Others"
        };
        final List<String> productList = new ArrayList<>(Arrays.asList(product));
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_product, productList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_product);
        SelectYourProductSpinner.setAdapter(spinnerArrayAdapter);
        SelectYourProductSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + "selected", Toast.LENGTH_LONG).show();
                    FlagProduct = true;
                    if (position == 5) {
                        OthersSpinner.setEnabled(true);
                        serialNo.setEnabled(true);
                        FlagSerialNo = true;
                        FlagOthers = true;
                        OthersSpinner.setVisibility(View.VISIBLE);
                        serialNo.setVisibility(View.VISIBLE);
                        ServiceRequestSerialNumber.setVisibility(View.VISIBLE);
                    } else {
                        OthersSpinner.setEnabled(false);
                        serialNo.setEnabled(false);
                        OthersSpinner.setVisibility(View.GONE);
                        serialNo.setVisibility(View.GONE);
                        ServiceRequestSerialNumber.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        String[] problem = new String[]{
                "Select your problem",
                "Break down call / unit not work",
                "Installation",
                "Re Installation",
                "De Installation",
                "Warranty PM call",
                "AMC PM call",
                "Transit damage",
                "Overload",
                "Other problem"
        };
        final List<String> problemList = new ArrayList<>(Arrays.asList(problem));
        final ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, R.layout.spinner_problem, problemList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_problem);
        SecectYourProblemSpinner.setAdapter(spinnerArrayAdapter1);
        SecectYourProblemSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + "selected", Toast.LENGTH_LONG).show();
                    FlagProblem = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        String[] product1 = new String[]{
                "Other options",
                "ac",
                "Transformer",
                "Voltage stabilizer",
                "Inverter",
                "Domestic inverter",
                "Solar"
        };
        final List<String> productList1 = new ArrayList<>(Arrays.asList(product1));
        final ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<String>(this, R.layout.others_spinner, productList1) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter3.setDropDownViewResource(R.layout.others_spinner);
        OthersSpinner.setAdapter(spinnerArrayAdapter3);
        OthersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + "selected", Toast.LENGTH_LONG).show();
                    FlagOthers = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private class MyAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params){ try {
            URL url = new URL(link);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Product", "ac");
            jsonParam.put("Product Serial Number", "213");
            jsonParam.put("Contact # Mobile", "9988776655");
            jsonParam.put("Problem", "De Installation");

            DataOutputStream out = new DataOutputStream(urlConnection.getOutputStream());
            // out.write(Integer.parseInt(jsonParam.toString()));
            out.write(Integer.parseInt(URLEncoder.encode(jsonParam.toString(), "UTF-8")));
            int HttpResult = urlConnection.getResponseCode();
            Log.d("Result", String.valueOf(HttpResult));
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                System.out.println("" + sb.toString());
                Log.d("Result", sb.toString());

            } else {
                System.out.println(urlConnection.getResponseMessage());
            }
            out.close();

            //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
            return null;
        }

    }

}